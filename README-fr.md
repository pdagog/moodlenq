Comment créer et analyser une enquête d'évaluation Moodle
=========================================================

* [English version](README.md)

Vous voulez faire une enquête d'évaluation de votre cours en
utilisant le module « Enquête »  de Moodle ?
Vous voulez construire **facilement** et **rapidement** votre
liste de questions, sans perdre votre temps à cliquer dans les
divers fenêtres de dialogues de Moodle ?
Vous voulez obtenir un **document final de qualité** incluant
vos propres commentaires ?
Alors, **moodlenq.py** est ce que dont vous avez besoin.


Ce document explique comment utiliser le programme `moodlenq.py` pour :
1. créer facilement une enquête Moodle (3.0)
2. générer un document complet avec des graphiques

Suivez [ce lien](example-fr/eval.pdf) pour un exemple de document
issu de l'enquête.


Prérequis
---------

Le programme `moodlenq.py` est un script Python3. Pour l'exécuter,
vous avez besoin :
- d'une installation de Python 3 (le programme a été testé avec la version
    3.12, mais devrait fonctionner avec toute version de Python 3)
- du paquet Python [openpyxl](https://pypi.org/project/openpyxl/) pour lire
    le fichier spécification comme le fichier de résultats généré
    par Moodle
- du paquet Python [matplotlib](http://matplotlib.org/) pour générer
    les graphiques (testé avec la version 2.2.3, disponible sur Linux
    Debian avec le paquet `python3-matplotlib`)

Si vous ne souhaitez/pouvez pas installer ces prérequis, utilisez
l'image Docker `pdagog/moodlenq` par exemple avec :

```
docker run --rm --volume "$PWD":/mnt --workdir /mnt -u 1000:1000 -i -t pdagog/moodlenq
```

De plus, vous aurez besoin de `pdflatex` (obtenu à partir de n'importe
quelle distribution sérieuse de TeX).

License
-------

Voir le [fichier LICENSE](LICENSE).

Les étapes
----------

Cette section donne un aperçu des étapes nécessaires pour construire
et analyser une enquête Moodle.


### Étape 0 - Télécharger ce dépôt

Lancez la commande :

```
git clone https://gitlab.com/pdagog/moodlenq.git
```

ou téléchargez le [fichier ZIP depuis
gitlib](https://gitlab.com/pdagog/moodlenq/-/archive/master/moodlenq-master.zip)
puis extrayez les fichiers.


### Étape 1 - Construire la spécification

La première étape est de construire la spécification de l'enquête dans
un fichier Excel. Voir la
[section suivante](#fichier-de-spécification) pour une description du
format du fichier de spécification. Vous pouvez également consulter un
[exemple](example-fr/spec.xlsx).

En supposant que le fichier de spécification est nommé `spec.xlsx`,
lancez la commande suivante pour générer le fichier (nommé `eval.xml`
ici) d'import XML pour Moodle :

```
./moodlenq.py xml -o eval.xml spec.xlsx
```

Vous pouvez maintenant importer ce fichier XML sur Moodle (cliquez sur
l'enquête, puis sélectionnez l'onglet `Modèles`, puis sélectionnez
`Importer des questions`), et vérifiez comment votre fichier est importé
avec l'onglet `Modifier les questions`.


### Étape 2 - Annoncer l'enquête auprès des étudiants

L'étape suivante est l'annonce de la disponibilité de l'enquête auprès
des étudiants. Laissez-leur du temps, et envoyez-leur une relance avant
de fermer l'enquête.


### Étape 3 - Créer un squelette LaTeX

Pendant ce temps, vous pouvez créer un fichier contenant un squelette
de document LaTeX à partir de la spécification.

```
./moodlenq.py skel -o eval.tex spec.xlsx
```

Vous pouvez modifier le fichier `eval.tex` généré suivant vos goûts
et les données fournies par les étudiants. Toutefois, vous ne pouvez
pas encore compiler ce fichier, tant que les graphiques et les données
ne sont pas encore générés par l'étape suivante.


### Étape 4 - Exporter les réponses et générer le document

L'étape finale est l'export des données générées par l'enquête avec
Moodle : dans l'onglet `Analyse`, cliquez sur le bouton `Exporter vers
Excel` et utilisez la commande :

```
./moodlenq.py result spec.xlsx feedback.xlsx
```

Le programme génère beaucoup de fichiers PDF et TEX (un par question)
dont les noms commencent par `q` (de telle manière que vous puissiez
les supprimer tous en une seule commande). Ces fichiers sont tous inclus
par le fichier `eval.tex` généré (dans l'étape 3).

Vous pouvez maintenant exécuter :

```
pdflatex eval.tex
```

pour obtenir le fichier final. Vous pouvez répéter cette étape 4
autant de fois que nécessaire.


Fichier de spécification
------------------------

La première étape est d'écrire la spécification de l'enquête dans un
fichier Excel (voir un [exemple ici](example-fr/spec.xlsx)).
Ce fichier doit contenir une feuille et les colonnes suivantes :

| Colonne | Description |
| :----: | :--------- |
|   A    | type (voir ci-après)       |
|   B    | attributs (voir ci-après) |
|   C    | texte de la question pour Moodle |
|   D    | texte de la question pour LaTeX |
|  E et suivantes | couples (légende, couleur) sur 2 colonnes consécutives |


La spécification commence réellement à la première ligne dont la
colonne A correspond à un type valide. Ceci vous permet de mettre des
commentaires (tels que les titres des colonnes) dans la ou les premières
lignes du fichier.


### Colonne A - type

Le **type** correspond à :

| Type         | Description | Attributs supportés |
| :----------: | :---------- | :------------------- |
| `lang`       | ce pseudo-type spécifie (via l'attribut en colonne B) la langue à utiliser dans le document final | `fr`, `en` |
| `title`      | spécifie un titre dans l'enquête | (aucun) |
| `text`       | spécifie une ligne de texte (sans réponse) dans l'enquête | `bold` |
| `choice`     | spécifie un choix (via des boutons _radio_) entre les valeurs citées dans les colonnes E, G, I, etc. | `bold`, `req`, `hor`/`vert`, `noanswer` |
| `menu`       | spécifie un choix (via un menu déroulant) entre les valeurs citées dans les colonnes E, G, I, etc. | `req`, `noanswer` |
| `multichoice`| spécifie un choix multiple (via des boutons _check_) entre les valeurs citées dans les colonnes E, G, I, etc. | `bold`, `req`, `hor`/`vert`, `noanswer` |
| `comment`    | spécifie un commentaire multi-lignes | `req`, *ncol*`\|`*nrow* |
| `line`       | spécifie un commentaire court sur une seule ligne | `req`, *max*`\|`*width* |
| `numeric`    | spécifie une valeur numérique | `req`, *min*`\|`*max* |


### Colonne B - attributs

Les **attributs** disponibles (en colonne B) sont :

| Attribut | Description |
| :-------: | :---------- |
| `fr` ou `en` | pour `lang` seulement : indique la langue à utiliser dans les textes |
| `req`     | réponse requise |
| `bold`    | choix (ou texte) en gras |
| `hor`     | les réponses de `choice` et `multichoice` sont présentées horizontalement (défaut) |
| `vert`    | les réponses de `choice` et `multichoice` sont présentées verticalement |
| `noanswer`| montre la valeur spéciale `pas de réponse` comme un choix possible |
| *ncol*`\|`*nrow* | pour le type `comment`, taille de la zone de texte définie comme *ncol* colonnes by *nrow* lignes |
| *max*`\|`*width* | pour le type `line`, taille de la ligne définie comme *max* caractères affichés dans une ligne de *width* colonnes |
| *min*`\|`*max* | pour le type `numeric`, valeurs admises entre *min* et *max* |
| `cumhbar=`*clef* | groupe plusieurs `choice` dans un simple histogramme (horizontal) à barres empilées |
| `cumvbar=`*clef* | groupe plusieurs `choice` dans un simple histogramme (vertical) à barres empilées |
| `rotlbl=`*angle* | angle pour la rotation des légendes d'axes |
| `reverse=`*1 ou 0* | inverse ou non l'ordre des éléments dans les barres empilées |

Les attributs non reconnus sont silencieusement ignorés.


### Colonne C and D - Textes des questions

La colonne C spécifie le texte de la question à utiliser dans l'enquête
Moodle (incluant des balises HTML si nécessaire).

Vous pouvez utiliser un texte différent dans les fichiers générés
pour LaTeX : par défaut, le texte est le même que celui utilisé
pour Moodle. Toutefois, si ce texte inclut des balises HTML ou si vous
préférez les titres plus courts (particulièrement utile pour les
titres des graphiques générés), vous pouvez inclure dans la colonne
D un texte différent.


### Colonnes E et suivantes - Légendes et couleurs

Les colonnes suivantes sont utilisées par paires : la première colonne est
le texte de la légende et la suivante est la couleur.

Les couleurs sont spécifiées de la manière suivante :

| Couleur | Description |
| :---: | :---------- |
| *nom de couleur* | utiliser ce nom |
| `#rrggbb` | utiliser la valeur numérique (rouge, vert, bleu) |
| (vide) | utiliser la couleur par défaut pour cet élément |
| `/`   | utiliser la couleur obtenue par un dégradé entre les couleurs environnantes |


### Exemple

Cet exemple montre une spécification fictive:

| A             | B          | C | D | E | F | G | H | I | J |
|:------------- |:---------- |:--|:--|:--|:--|:--|:--|:--|:--|
| `Type`        | `Attr`     | `Titre Moodle`       | `Titre LaTeX` | `Légende1` | `Couleur1` | `Légende2` | `Couleur2` | ... | ... |
| `lang`        | `en`       |                      |              | | | | | | |
| `title`       |            | `À propos de vous`   |              | | | | | | |
| `text`        | `bold`     | `Répondez honnêtement`    |              | | | | | | |
| `choice`      | `req bold` | `Êtes-vous heureux`      | `Bonheur`  | `Oui` | `green`  | `Peut-être`  | `/` | `Non`  | `red` |
| `multichoice` |            | `Qu'est-ce qui vous motive` | `Motivation` | `Argent` | | `Connaissance` | | | |
| `choice`      | `cumhbar=foo` | `Difficulté du chapitre 1 (0: très facile, 2: très difficile)` | `Chap 1`  | `0` | `green`  | `1`  | `/` | `2`  | `red` |
| `choice`      | `cumhbar=foo` | `Difficulté du chapitre 2 (0: très facile, 2: très difficile)` | `Chap 2`  | `0` | `green`  | `1`  | `/` | `2`  | `red` |
| `choice`      | `cumhbar=foo` | `Difficulté du chapitre 3 (0: très facile, 2: très difficile)` | `Chap 3`  | `0` | `green`  | `1`  | `/` | `2`  | `red` |
| `comment`     | `80\|10`    | `Expliquez pourquoi`       |              | | | | | | |

Vous pouvez aussi consulter un [exemple complet ici](example-fr/spec.xlsx).


Release notes
-------------

### v2.2

* correction d'un bug dans un message

### v2.1

* correction du bug où une légende est un nombre (type numérique)

### v2.0

* ajout de l'option `-V` pour obtenir le numéro de version
* utilisation de **openpyxl** au lieu de l'obsolète **xlrd** :
  en conséquence, les anciens fichiers `.xls` (Excel <= 2007) ne
  sont plus reconnus, seuls les fichiers `.xlsx` (Excel >= 2010)
  le sont dorénavant
    * on peut utiliser `libreoffice --convert-to xlsx spec.xls` pour
      convertir automatiquement les vieux fichiers `.xls` en `.xlsx`
* correction de certaines expressions régulières non acceptées par les
  versions récentes de Python

### v1.4

* titres des graphiques sur plusieurs lignes au besoin

### v1.3

* espacement des paragraphes respecté dans les \\figcomment du document LaTeX

### v1.2

* ajout de l'attribut `reverse=`(uniquement pour `cumhbar=` et `cumvbar=` à l'heure actuelle) 

### v1.1

* ajout des histogrammes groupés (attributs `cumhbar=` et `cumvbar=`)
* ajout de l'attribut `rotlbl=` (uniquement pour `cumhbar=` et `cumvbar=` à l'heure actuelle)

### v1.0

* Version initiale
