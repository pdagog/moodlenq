#!/usr/bin/env python3

#
# Required Python packages (Debian names):
#   - python3-openpyxl
#   - python3-matplotlib (which may need python3-tk)
#
# Needed files:
#   - this program
#   - spec.xls (specification to build, see below)
#   - feedback.xlsx (evaluation from students, to get from Moodle)
#
# How to make an evaluation survey with Moodle?
#
# Step 1 :
# - Build a feedback specification into the spec.xls file
# - Run this program to build the XML survey to import with Moodle
#       ./moodlenq.py xml -o eval.xml spec.xls
# - Import the generated XML file (eval.xml)
#
# Step 2 :
# - Announce the questionnaire to students, and leave some time for them
#   to fill in the questionnaire
# - Send a reminder to students
#
# Step 3:
# - Create the skeleton LaTeX file from the specification
#       ./moodlenq.py skel -o eval.tex spec.xls
# - Customize the generated LaTeX file (eval.tex)
# - Note that this skeleton file includes many other q*.tex (for comments)
#   or q*.png (for pie or bar charts) which do not exist yet (see step 4
#   below)
#
# Step 4:
# - Export answers (Excel export) to get the file feedback.xlsx
#       Only the first sheet of the file will be used by this program,
#       the second sheet contains detailed answers
# - Create all files to be included by the latex master file:
#       ./moodlenq result spec.xls feedback.xlsx
# - Run pdflatex to get the final file
#       pdflatex eval.tex
# - Repeat step 4 as many times as necessary
#
#
# Specification file format:
# - column A : type
#       lang            special type for language specification
#       title           provide a title (no input from user)
#       text            provide a line of text (no input from user)
#       choice          choice in a list (radio buttons)
#       menu            choice in a list (menu)
#       multichoice     multiple choice (check buttons)
#       comment         multi-line comment (text area)
#       line            single-line comment (input)
#       numeric         numeric value (input)
# - column B : attributes (keyword list separated by space)
#       Possible attributes are specific to each type:
#           fr or en    specified language (lang only)
#           req         required answer (all types except lang, title and text)
#           bold        bold text (choice / multichoice)
#           hor or vert horizontal [default] or vertical (choice / multichoice)
#           noanswer    add « no answer » in possible answers (choice)
#           n|m         n cols x m rows in textarea (comment)
#           n|m         n chars max in an m cols field (line)
#           min|max     interval for value (numeric)
# - column C : question text (for Moodle)
# - column D : question text (for LaTeX output)
#       If the cell is empty, text for column C is used in LaTeX output
# - following columns: couples legend/color
#       legend: legend
#       color: color (see matplotlib for colors) or "/" (real color is
#           computed from neighboring columns) or nothing (default color
#           is used.
#

import os
import sys
import argparse
import re
import html
import locale

import openpyxl

import matplotlib.pyplot as plt

allowed_types = ['title', 'text', 'choice', 'multichoice', 'menu', 'comment', 'line', 'numeric']

# Default colors
defcolors = ['red', 'green', 'steelblue', 'cyan', 'blueviolet', 'coral', 'darkgreen', 'cornflowerblue', 'goldenrod', 'firebrick']

version = '2.2'

##############################################################################
# Translations
##############################################################################

# default language should be described in col A
messages = {
    'C': 'en',
    'en': {
        # Cell (in the first column) signalling the end of header in the
        # feedback.xlsx file
        'lastcell': 'Label',
        'nrep': 'Feedback',
        'latex_prologue': """\\documentclass {article}
            \\usepackage [french] {babel}
            \\usepackage [utf8] {inputenc}
            \\usepackage [T1] {fontenc}
            \\raggedbottom
            \\usepackage [a4paper, margin=20mm] {geometry}
            \\usepackage {graphicx}
            \\usepackage {hyperref}
            \\frenchspacing
            \\parskip=2mm
            \\parindent=0mm
            \\usepackage {mathpazo}
            \\usepackage [scaled] {helvet}
            \\usepackage {courier}

            \\setkeys {Gin} {keepaspectratio}

            \\newcommand {\\figcomment} [2] {
                \\parbox [c] {.50\\linewidth} {\\parskip=1.5mm #2} %
                \\hfill %
                \\parbox [c] {.50\\linewidth} {%
                    \\includegraphics [width=\\linewidth] {#1}} %
                \\par
            }

            \\newcommand {\\doublefigure} [2] {
                \\includegraphics [width=.50\\linewidth] {#1} %
                \\hfill %
                \\includegraphics [width=.50\\linewidth] {#2} %
                \\par
            }

            \\begin {document}
            Strasbourg University
                \\hfill
                Course XXX
                \\\\
            Licence d'informatique -- Semester 7
                \\hfill
                1788-1789/Spring
                \\\\
            \\par
            \\begin {center}
                 \\huge\\textbf {Feedback on XXX course}
            \\end {center}

            \\section {Introduction}
            \\footnote {This survey as well as this document have been
            prepared thanks to the \\texttt {moodlenq} software, available
            on \\url {https://gitlab.com/pdagog/moodlenq} with a free
            software license.}
            """,
        'latex_epilogue': """\\end {document}
            """,
        'latex_title': """\\section {%TEXT%}
            """,
        'latex_text': """%TEXT%
            """,
        'latex_choice': """\\figcomment {%FILENAME%} {
                Useful comment on %TITLE%
            }
            """,
        'latex_comment': """\\subsection {%TITLE%}
            \\input {%FILENAME%}
            """,
        'latex_startcomment': """\\begin {itemize} \\small \\itshape
            """,
        'latex_endcomment': """\\end {itemize}
            """,
        'latex_onecomment': """\\item %COMMENT%
            """,
        'latex_nocomment': """{\\small \\textit {(no answer)}}
            """,
    },
    'fr': {
        'lastcell': 'Étiquette',
        'nrep': 'Réponses envoyées',
        'latex_prologue': """\\documentclass {article}
            \\usepackage [french] {babel}
            \\usepackage [utf8] {inputenc}
            \\usepackage [T1] {fontenc}
            \\raggedbottom
            \\usepackage [a4paper, margin=20mm] {geometry}
            \\usepackage {graphicx}
            \\usepackage {hyperref}
            \\frenchspacing
            \\parskip=2mm
            \\parindent=0mm
            \\usepackage {mathpazo}
            \\usepackage [scaled] {helvet}
            \\usepackage {courier}

            \\setkeys {Gin} {keepaspectratio}

            \\newcommand {\\figcomment} [2] {
                \\parbox [c] {.50\\linewidth} {\\parskip=1.5mm #2} %
                \\hfill %
                \\parbox [c] {.50\\linewidth} {%
                    \\includegraphics [width=\\linewidth] {#1}} %
                \\par
            }

            \\newcommand {\\doublefigure} [2] {
                \\includegraphics [width=.50\\linewidth] {#1} %
                \\hfill %
                \\includegraphics [width=.50\\linewidth] {#2} %
                \\par
            }

            \\begin {document}
            Université de Strasbourg
                \\hfill
                UE de XXX
                \\\\
            Licence d'informatique -- Semestre 7
                \\hfill
                1788-1789/Printemps
                \\\\
            \\par
            \\begin {center}
                 \\huge\\textbf {Évaluation de l'UE « XXX »}
            \\end {center}

            \\section {Présentation}
            \\footnote {Ce questionnaire ainsi que ce document ont été
            préparés grâce au logiciel \\texttt {moodlenq} disponible
            sur \\url {https://gitlab.com/pdagog/moodlenq} en licence
            libre.}
            """,
        'latex_epilogue': """\\end {document}
            """,
        'latex_title': """\\section {%TEXT%}
            """,
        'latex_text': """%TEXT%
            """,
        'latex_choice': """\\figcomment {%FILENAME%} {
                Commentaire utile sur %TITLE%
            }
            """,
        'latex_comment': """\\subsection {%TITLE%}
            \\input {%FILENAME%}
            """,
        'latex_startcomment': """\\begin {itemize} \\small \\itshape
            """,
        'latex_endcomment': """\\end {itemize}
            """,
        'latex_onecomment': """\\item %COMMENT%
            """,
        'latex_nocomment': """{\\small \\textit {(aucune réponse)}}
            """,
    },
}

def setlang (l):
    global lang

    if l is None:
        l = messages ['C']

    if not (l in messages):
        newl = messages ['C']
        print ("WARNING: Language '{}' not supported. Using '{}'".format (l, newl), file=sys.stderr)
        l = newl

    lang = l

def translate (what, **kwargs):
    if what in messages [lang]:
        msg = messages [lang][what]
    else:
        error ("Key '{}' not found in 'messages' dict".format (what))

    msg = re.sub ('^\\s+', '', msg, flags=re.MULTILINE)

    for k in kwargs:
        pattern = '%' + k.upper () + '%'
        msg = re.sub (pattern, kwargs [k], msg)

    return msg

##############################################################################
# Utilities
##############################################################################

def error (msg):
    print (msg, file=sys.stderr)
    sys.exit (1)

def strip (txt):
    """
    Remove spaces and HTML tags
    """

    txt = re.sub ('<br\\s*/>', '\n', txt)   # keep newlines in source text
    txt = re.sub ('<[^>]+>', '', txt)       # remove all remaining formats
    # txt = re.sub ('\\s+', ' ', txt)
    txt = re.sub ('^\\s+', '', txt)
    txt = re.sub ('\\s+$', '', txt)
    return txt

def texescape (txt):
    """
    Convert special characters to LaTeX characters
    Note: this function is not complete, it has just been tested on some
        examples
    """

    txt = re.sub ('\\\\', '{\\\\\\\\textbackslash}', txt)
    txt = re.sub ('#', '\\#', txt)
    txt = re.sub ('&', '\\&', txt)
    txt = re.sub ('\\$', '\\$', txt)
    txt = re.sub ('_', '\\_', txt)
    txt = re.sub ('%', '\\%', txt)
    return txt

def attr_cumbar (attr):
    for a in attr:
        l = a.split ('=')
        if len(l) == 2 and l[0] == 'cumhbar' or l[0] == 'cumvbar':
            return l
    return None

def attr_rotlbl (attr):
    for a in attr:
        l = a.split ('=')
        if len(l) == 2 and l[0] == 'rotlbl':
            return int(l[1])
    return 0

def attr_reverse (attr):
    for a in attr:
        l = a.split ('=')
        if len(l) == 2 and l[0] == 'reverse':
            if l[1] in ['0', 'no', 'false']:
                return False
            else:
                return True
    return False

##############################################################################
# Specification file reading
##############################################################################

def read_spec (specfile):
    """
    Result is an array indexed by index values
        spec ['idx']: list of indexes
        spec [i] =  (etiq, type, attr, title_moodle, title_latex, legends)
    """

    #
    # Open the specification file
    #

    wb = openpyxl.load_workbook (filename = specfile, read_only = True)
    if len (wb.sheetnames) > 1:
        name = wb.sheetnames [0]
        print ('Using first sheet ({}) from {}'.format (name, specfile))

    sheet = wb.worksheets [0]

    #
    # Search the first useful line: we take the first row n (n <= 10)
    # where the An cell includes a recognized type.
    #

    curetiq = 1
    spec = {}
    spec ['idx'] = []               # index list
    nl = 0
    for row in sheet.iter_rows (values_only = True):
        line = ['' if x is None else x for x in row]
        nl += 1
        typ = line [0]
        if typ == 'lang':
            setlang (line [1].strip ())
        elif typ in allowed_types:
            etiq = None

            attr = re.sub ('\\s+', ' ', line [1].strip ()).split ()
            title_moodle = line [2].strip ()
            title_latex = line [3].strip ()
            if title_latex == '':
                title_latex = title_moodle
            legendes = []
            if typ in ['choice', 'multichoice', 'menu', 'comment', 'line', 'numeric']:
                etiq = curetiq
                curetiq += 1
                idx = str (etiq)
                if typ in ['choice', 'multichoice', 'menu']:
                    legendes = legend_colors (line [4:], specfile, nl+1)

            else:
                idx = '_' + str (nl + 1)

            spec [idx] = (etiq, typ, attr, title_moodle, title_latex, legendes)
            spec ['idx'].append (idx)

        else:
            if nl > 2:
                print ('Invalid row {} ({}) in {}'.format (nl, line, specfile), file=sys.stderr)

    return spec

def legend_colors (leg, fname, line):
    """
    Replace missing colors in specified legends
    """
    tabcol = {}
    tableg = {}

    idx = 0                     # index of current couple <legend,color>
    lastcol = None              # index of last known color
    cur = 'legend'              # current state ('legend' or 'color')

    for t in leg:
        # clean-up t
        if type(t) is str:
            t = strip (t)
        elif type (t) is float:
            if t == int (t):
                t = str (int (t))
            else:
                t = str (t)

        if cur == 'legend':               # it's a legend
            if t == '':
                break                       # stop at first empty legend
            tableg [idx]  = t
            cur = 'color'
        else:                             # it's a color specification
            if t == '/':                    # '/' : shading
                if lastcol is None:
                    print ('{}/{}: No color for item {}'.
                                            format (fname, line, idx+1))
            else:                           # empty color or specified color
                if t == '':                 # empty: default color
                    t = defcolors [idx]
                tabcol [idx] = t
                if lastcol is not None and idx - lastcol > 1:
                    tabcol = fill_shade (tabcol,
                                            lastcol + 1, idx - 1,
                                            tabcol [lastcol], t)
                lastcol = idx

            cur = 'legend'
            idx += 1

    if cur == 'color':                      # last cell is a legend
        tabcol [idx] = defcolors [idx]
        if lastcol is not None and idx - lastcol > 1:
            tabcol = fill_shade (tabcol,
                                    lastcol + 1, idx - 1,
                                    tabcol [lastcol], t)
        lastcol = idx
        idx += 1

    if lastcol is not None and idx - lastcol > 1:
        print ('{}/{}: No end color for shading'.format (fname, line), file=sys.stderr)

    lcouples = []
    for i in range (idx):
        lcouples.append ((tableg [i], tabcol [i]))

    return lcouples

def fill_shade (tabcol, imin, imax, col1, col2):
    """
    Compute shaded colors (between colors col1 and col2) and place them
    in tabcol [imin:imax]
    """

    nshade = imax - imin + 2
    (rmin, vmin, bmin) = read_color (col1)
    (rmax, vmax, bmax) = read_color (col2)

    (r, v, b) = (rmin, vmin, bmin)
    idx = imin

    for i in range (nshade):
        r += (rmax - rmin) / nshade
        v += (vmax - vmin) / nshade
        b += (bmax - bmin) / nshade
        tabcol [imin + i] = write_color (r, v, b)

    return tabcol

def read_color (col):
    m = re.match ('^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})', col, re.IGNORECASE)
    if m is not None:
        r = int (m.group (1), 16)
        v = int (m.group (2), 16)
        b = int (m.group (3), 16)
    else:
        r = 255
        v = 255
        b = 255
    return (r, v, b)

def write_color (r, v, b):
    return '#{:02x}{:02x}{:02x}'.format (int (r), int (v), int (b))

##############################################################################
# Generates the XML import file for Moodle
##############################################################################

def xml_bold (e, attr):
    if 'bold' in attr:
        e = '<b>' + e + '</b>'
    return e

def xml_get_req (attr):
    if 'req' in attr:           # required
        opt_req = '1'
    else:
        opt_req = '0'
    return opt_req

def xml_get_dim (attr, default):
    dim = '' if default is None else default
    for a in attr:
        if re.fullmatch ('\\d+\\|\\d+', a) is not None:
            dim = a
            break
    return dim

def xml_prologue ():
    t = """<?xml version="1.0" encoding="UTF-8" ?>
<FEEDBACK VERSION="200701" COMMENT="XML-Importfile for mod/feedback">
<ITEMS>
    """
    return t

def xml_epilogue ():
    t = "</ITEMS></FEEDBACK>"
    return t

itemid = 0

def xml_item (typ, req, txt, label, pres, opt):
    global itemid
    itemid += 1
    t = '<ITEM TYPE="{}" REQUIRED="{}">\n'.format (typ, req)
    t += '<ITEMID><![CDATA[{}]]></ITEMID>\n'.format (itemid)
    t += '<ITEMTEXT><![CDATA[{}]]></ITEMTEXT>\n'.format (txt)
    t += '<ITEMLABEL><![CDATA[{}]]></ITEMLABEL>\n'.format (label)
    t += '<PRESENTATION><![CDATA[{}]]></PRESENTATION>\n'.format (pres)
    t += '<OPTIONS><![CDATA[{}]]></OPTIONS>\n'.format (opt)
    t += '<DEPENDITEM><![CDATA[{}]]></DEPENDITEM>\n'.format (0)
    t += '<DEPENDVALUE><![CDATA[{}]]></DEPENDVALUE>\n'.format (0)
    t += '</ITEM>\n'
    return t

def xml_title (title, attr):
    h = '<h2 style="color: rgb(255,255,255); background-color: rgb(102,102,102); padding: 5px;"><span>'
    h += title
    h += '</span></h2>'
    t = xml_item ('label', 0, 'label', '', h, '')
    return t

def xml_text (title, attr):
    h = '<p>' + xml_bold (title, attr) + '</p>'
    t = xml_item ('label', 0, 'label', '', h, '')
    return t

def xml_choice (typ, etiq, title, attr, legends):
    if typ == 'choice':
        mood_type = 'r>>>>>'
    elif typ == 'multichoice':
        mood_type = 'c>>>>>'
    elif typ == 'menu':
        mood_type = 'd>>>>>'

    opt_req = xml_get_req (attr)

    opt_hor = ''
    if typ in ['choice', 'multichoice']:
        if 'vert' not in attr:      # vertical ('hor' for horizontal)
            opt_hor = '<<<<<1'

    if 'noanswer' in attr:          # display "no answer"
        opt_noans = ''
    else:
        opt_noans = 'h'             # hide "no answer"

    pres = ''
    for (l,c) in legends:
        if pres != '':
            pres += '\r|'
        if not isinstance(l, str):
            l = str(l)
        pres += xml_bold (l, attr)
    pres = mood_type + pres + opt_hor
    t = xml_item ('multichoice', opt_req, title, etiq, pres, opt_noans)
    return t

def xml_comment (etiq, title, attr):
    opt_req = xml_get_req (attr)
    pres = xml_get_dim (attr, '80|10')
    t = xml_item ('textarea', opt_req, title, etiq, pres, '')
    return t

def xml_line (etiq, title, attr):
    opt_req = xml_get_req (attr)
    pres = xml_get_dim (attr, '80|10')
    t = xml_item ('textfield', opt_req, title, etiq, pres, '')
    return t

def xml_numeric (etiq, title, attr):
    opt_req = xml_get_req (attr)
    pres = xml_get_dim (attr, None)
    t = xml_item ('numeric', opt_req, title, etiq, pres, '')
    return t

#
# Write the XML import file for Moodle
#

def do_xml (args):
    verbose = args.verbose
    output = args.output
    specfile = args.specfile

    spec = read_spec (specfile)

    if output is not None:
        xmlfp = open (output, mode='w')
    else:
        xmlfp = sys.stdout

    print (xml_prologue (), file=xmlfp) ;

    for idx in spec ['idx']:
        (etiq, typ, attr, title_moodle, title_latex, legendes) = spec [idx]
        basename = "WARNING : no label for {}".format (title_moodle)

        if typ == 'title':
            t = xml_title (title_moodle, attr)

        elif typ == 'text':
            t = xml_text (title_moodle, attr)

        elif typ in ['choice', 'multichoice', 'menu']:
            t = xml_choice (typ, etiq, title_moodle, attr, legendes)

        elif typ == 'comment':
            t = xml_comment (etiq, title_moodle, attr)

        elif typ == 'line':
            t = xml_line (etiq, title_moodle, attr)

        elif typ == 'numeric':
            t = xml_numeric (etiq, title_moodle, attr)

        else:
            print ('Invalid type {}'.format (typ), file=sys.stderr)
            sys.exit (1)

        print (t, file=xmlfp)

    print (xml_epilogue (), file=xmlfp) ;


##############################################################################
# LaTeX skeleton
##############################################################################

def latex_prologue ():
    t = translate ('latex_prologue')
    return t

def latex_epilogue ():
    t = translate ('latex_epilogue')
    return t

def latex_title (txt):
    t = translate ('latex_title', text=txt)
    return t

def latex_text (txt):
    t = translate ('latex_text', text=txt)
    return t

def latex_choice (title, filename):
    t = translate ('latex_choice', filename=filename, title=title)
    return t

def latex_comment (title, filename):
    t = translate ('latex_comment', filename=filename, title=title)
    return t

def latex_basename (etiq):
    return 'q' + str (etiq)

#
# Write the LaTeX master file
#

def do_skel (args):
    setlang (None)
    verbose = args.verbose
    output = args.output
    specfile = args.specfile
    cumbar = {}

    spec = read_spec (specfile)

    if output is not None:
        skelfp = open (output, mode='w')
    else:
        skelfp = sys.stdout

    print (latex_prologue (), file=skelfp) ;

    for idx in spec ['idx']:
        (etiq, typ, attr, title_moodle, title_latex, legendes) = spec [idx]
        basename = "WARNING : no label for {}".format (title_moodle)
        if etiq is not None:
            basename = latex_basename (etiq)
        t = None
        if typ in ['title']:
            t = latex_title (texescape (title_latex))

        elif typ in ['text']:
            t = latex_text (texescape (title_latex))

        elif typ in ['choice', 'multichoice', 'menu']:
            cb = attr_cumbar (attr)
            if cb is None:
                t = latex_choice (title_latex, basename)
            else:
                (orient, key) = cb
                if key not in cumbar:
                    t = latex_choice (title_latex, basename)
                    cumbar [key] = True

        elif typ in ['comment', 'line', 'numeric']:
            t = latex_comment (title_latex, basename)

        else:
            print ('Invalid type {}'.format (typ), file=sys.stderr)
            sys.exit (1)

        if t is not None:
            print (t, file=skelfp)

    print (latex_epilogue (), file=skelfp) ;


##############################################################################
# Result file analyis
##############################################################################

def gather_values (reponses, hdr, legends):
    values = []
    for idxrep in range (len (reponses)):
        if reponses [idxrep] == '':
            break
        v = int (reponses [idxrep])

        leg = strip (hdr [idxrep])
        col = defcolors [idxrep]

        if idxrep < len (legends):
            (specl, specc) = legends [idxrep]
        else:
            (specl, specc) = ('', '')

        if isinstance (specl, (int, float)):
            specl = str (specl)
        if strip (specl) != '':
            leg = specl
        if strip (specc) != '':
            col = specc

        values.append ((v, leg, col))
    return values

def split_values (values, check0):
    """
    Split values returned by gather_values into labels[], colors[] and sizes[]
    """
    labels = []
    colors = []
    sizes = []
    for (v, leg, col) in values:
        if check0 and v == 0:
            continue
        labels.append (leg)
        colors.append (col)
        sizes.append (v)
    return (labels, colors, sizes)

def best_ticks (max):
    """
    Given a maximum number for an axis, returns the 'best' ticks value
    """
    if max < 5:
        return list(range (0, max+1))

    found = False
    # try all values between max and max + 25%
    for m in range (max, int (max*1.25 + 0.9)):
        # try to decompose 
        for n in range (4, 8):
            if m % n == 0:
                max = m
                sep = int (max / n)
                found = True
                break
        if found:
            break
    if not found:
        print ('panic, max={}'.format (max), file=sys.stderr)
        sys.exit (1)

    return list (range (0, max+1, sep))

def get_row (sheet, num):
    # Get row (1..n)
    row = sheet [num]
    # Get cell values instead of cell objects
    row = [x.value for x in row]
    # Get '' instead of None for values
    row = ['' if x is None else x for x in row]
    return row

def process_result (xlsfile, spec):
    """
    Read a feedback.xlsx exported from Moodle, giving survey results
    and, more particularly, in the specified sheet.
    We are looking for 2 cells:
    - Enquête: <n>: provide the number of answers
    - Etiquette: we start analyzing answers with the following line
    """

    lastcell = translate ('lastcell')
    nreptxt = translate ('nrep')
    cumbar = {}

    #
    # Open the feedback.xlsx file
    #

    wb = openpyxl.load_workbook (filename = xlsfile, read_only = True)
    if len (wb.sheetnames) > 1:
        name = wb.sheetnames [0]
        print ('Using first sheet ({}) from {}'.format (name, xlsfile))

    sheet = wb.worksheets [0]

    #
    # Analyze sheet header by using first column
    #

    nrep = None
    mark = False            # end of header mark
    nl = 1                  # number of the first significant row
    for row in sheet.iter_rows(max_col = 1, values_only = True):
        cell = row [0]
        if cell is None:
            cell = ''
        regexp = '^' + nreptxt + '.*:\\s*(\\d+)'
        m = re.match (regexp, cell, re.IGNORECASE)
        if m is not None:
            nrep = int (m.group (1))
        else:
            m = re.match (lastcell, cell, re.IGNORECASE)
            if m is not None:
                mark = True
                nl += 1
                break
        nl += 1

    #
    # Check header
    #

    if nrep is None:
        error ('Number of answers not found in {}'.format (xlsfile))
    if not mark:
        error ('Cell "{}" not found in {}'.format (lastcell, xlsfile))

    #
    # Analyze answers to each question
    #

    adone = {}                     # avoid to repeat twice one question
    while nl <= sheet.max_row:
        row = get_row (sheet, nl)
        etiq = row [0]

        if etiq not in spec:
            error ('{}/{}: invalid label ({})'.format (xlsfile, nl+1, etiq))

        (etiqr, typ, attr, title_moodle, title_latex, legends) = spec [etiq]

        if etiq in adone:
            if typ in ['choice', 'multichoice', 'menu']:
                nl += 3
            else:
                nl += nrep + 1
            continue

        basename = latex_basename (etiq)

        title = row [1]
        if title != title_moodle:
            print ('{}/{}: non-consistant title'.format (xlsfile, nl+1), file=sys.stderr)
            print ('Found "{}", should be "{}"'.format (title, title_moodle), file=sys.stderr)

        if typ in ['choice', 'menu']:
            values = gather_values (get_row (sheet, nl + 1) [2:],
                                    row [2:], legends)

            cb = attr_cumbar (attr)
            if cb is None:
                # don't retain null values for pie chart
                (labels, colors, sizes) = split_values (values, True)

                # plt.figure (figsize=(10, 10))
                ti = plt.title (title_latex, wrap=True)
                (p,t,a) = plt.pie (sizes, labels=labels, colors=colors,
                                    labeldistance=1.03, autopct='%1.1f %%')
                plt.axis ('equal')
                ###print ('ylim = {}, xlim = {}', plt.ylim (), plt.xlim ())
                ###print ('title: ({},{})'.format (ti._x, ti._y))

                plt.draw ()
                plt.savefig (basename + '.pdf')
                plt.close ()

            else:
                #
                # Memorize cumbars, for further processing after the loop
                #

                (orient, key) = cb
                (labels, colors, sizes) = split_values (values, False)
                if key not in cumbar:
                    rotlbl = attr_rotlbl (attr)
                    rev = attr_reverse (attr)
                    cumbar[key] = (orient, rotlbl, rev, basename,
                                                labels, colors, [])
                cumbar [key][6].append ((title_latex, sizes))

            nl += 3

        elif typ == 'multichoice':

            rotlbl = attr_rotlbl (attr)
            values = gather_values (get_row (sheet, nl + 1) [2:],
                                    row [2:], legends)

            (labels, colors, sizes) = split_values (values, False)

            plt.title (title_latex)
            x = 1.0
            for (v, leg, col) in values:
                plt.bar ([x], [v], width=1, color=col, label=leg)
                x += 1
            plt.xticks (range (1, int (x+0.5)), labels, rotation=rotlbl)
            plt.savefig (basename + '.pdf')
            plt.close ()

            nl += 3

        elif typ in ['comment', 'line']:

            with open (basename + '.tex', mode='w') as texfd:
                nitems = 0
                for idx in range (nrep):
                    if idx != 0:
                        try:
                            row = get_row (sheet, nl + idx)
                        except IndexError:
                            row = ['', '', '']
                    rep = strip (html.unescape (row [2]))
                    if rep != '':
                        if nitems == 0:
                            m = translate ('latex_startcomment') ;
                            print (m, file=texfd)
                        r = texescape (rep)
                        m = translate ('latex_onecomment', comment=r)
                        print (m, file=texfd)
                        nitems += 1
                if nitems == 0:
                    m = translate ('latex_nocomment')
                    print (m, file=texfd)
                else:
                    m = translate ('latex_endcomment')
                    print (m, file=texfd)

            nl += nrep + 1

        elif typ == 'numeric':

            with open (basename + '.tex', mode='w') as texfd:
                print (get_row (sheet, nl + 1) [2], file=texfd)

            nl += 2

        else:
            error ('{}/{}: can\'t happen (type={})'.format (xlsfile, nl+1, typ))

        adone [etiq] = True
    
    #
    # Process cumulated histograms
    #

    for key in cumbar:
        (orient, rotlbl, rev, basename, labels, colors, allsizes) = cumbar [key]

        if rev:
            colors.reverse()

        if orient == 'cumvbar':
            #
            # vertical
            #

            nbars = len (allsizes)
            x = 1.0
            n = len (colors)
            lab = []

            for sz in allsizes:
                y = 0
                (title_latex, sizes) = sz
                if rev:
                    sizes.reverse ()
                lab.append (title_latex)
                for i, v in enumerate (sizes):
                    l = labels [i]
                    c = colors [i]
                    plt.bar ([x], [v], width=.8, bottom=[y], color=c, label=l)
                    y += v
                x += 1

            plt.grid (axis='y')
            plt.yticks (best_ticks (nrep))
            if rotlbl == 0:
                rotlbl = 'horizontal'
            plt.xticks (range (1, int (nbars+1.5)), lab, rotation=rotlbl)

        else:
            #
            # horizontal
            #

            nbars = len (allsizes)
            y = nbars
            n = len (colors)
            lab = []
            for sz in allsizes:
                x = 0
                (title_latex, sizes) = sz
                if rev:
                    sizes.reverse ()
                lab.insert (0, title_latex)
                for i, v in enumerate (sizes):
                    l = labels [i]
                    c = colors [i]
                    plt.barh ([y], [v], height=.8, left=[x], color=c, label=l)
                    x += v
                y -= 1

            plt.grid (axis='x')
            plt.xticks (best_ticks (nrep))
            plt.yticks (range (1, int (nbars+1.5)), lab, rotation=rotlbl)

            # legends = []
            # for i, v in enumerate (sizes):
                

        plt.savefig (basename + '.pdf')
        plt.close ()

    return


def do_result (args):
    setlang (None)
    verbose = args.verbose
    specfile = args.specfile
    xlsfile = args.feedback

    spec = read_spec (specfile)
    process_result (xlsfile, spec)

##############################################################################
# MAIN
##############################################################################

def main ():
    p = argparse.ArgumentParser (description='Evaluation survey')
    p.add_argument ('-v', '--verbose', action='store_true',
                                help='verbose messages')
    p.add_argument ('-V', '--version', action='version', version=version,
                                help='display version and exit')
    subp = p.add_subparsers (help='generation action')

    # moodlenq [-v] xml [-o enq.xml] spec.xls
    p_xml = subp.add_parser ('xml', help='XML generation for Moodle import')
    p_xml.add_argument ('-o', '--output', metavar='file', action='store',
                                help='enq.xml output file')
    p_xml.add_argument ('specfile', metavar='spec.xls',
                                help='spec.xls specification file')
    p_xml.set_defaults (func=do_xml)

    # moodlenq [-v] xkel [-o enq.tex] spec.xls
    p_skel = subp.add_parser ('skel', help='LaTeX master file generation')
    p_skel.add_argument ('-o', '--output', metavar='file', action='store',
                                help='eval.tex output file')
    p_skel.add_argument ('specfile', metavar='spec.xls',
                                help='spec.xls specification file')
    p_skel.set_defaults (func=do_skel)

    # moodlenq [-v] result spec.xls feedback.xls
    p_res = subp.add_parser ('result', help='Feedback analysis')
    p_res.add_argument ('specfile', metavar='spec.xls',
                                help='spec.xls specification file')
    p_res.add_argument ('feedback', metavar='feedback.xlsx',
                                help='feedback.xlsx output file from Moodle')
    p_res.set_defaults (func=do_result)

    args = p.parse_args ()

    if hasattr (args, 'func'):
        args.func (args)
    else:
        p.print_help ()
        sys.exit (1)

    sys.exit (0)

if __name__ == '__main__':
    main ()
