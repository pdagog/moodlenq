How to create and analyze a Moodle feedback survey
==================================================

* [Français](README-fr.md)

You want to get feedback on your course from your students using the
Moodle "feedback" module? You want to **easily** and **quickly build
your survey questions**, without loosing your time in mouse-clicking
in various dialogs? You want to get a **final high-quality document**
including your own comments? Then, **moodlenq.py** is what you need.


This document explains how to use the `moodlenq.py` program in order to:
1. easily create a Moodle (3.0) feedback survey
2. generate a complete document with some graphs

Follow [this link](example-en/eval.pdf) for an example output.

Prerequisites
-------------

The `moodlenq.py` program is a Python3 script. To run it, you need:
- Python 3 installation (tested with 3.12, should work with any Python
    3 installation)
- [openpyxl](https://pypi.org/project/openpyxl/) Python package to read
    the specification file as well as result file generated by Moodle
- [matplotlib](http://matplotlib.org/) Python package to generate
    graphs (tested with version 2.2.3, available as the
    `python3-matplotlib` Debian package)

If you don't want/cannot install those prerequisites, use the
Docker image `pdagog/moodlenq`, for example with:

```
docker run --rm --volume "$PWD":/mnt --workdir /mnt -u 1000:1000 -i -t pdagog/moodlenq
```

In addition, you need `pdflatex` (obtained from any serious TeX
distribution).

License
-------

See the [LICENSE file](LICENSE).

Steps
-----

This section provides an overview of the steps needed to build and
analyze a Moodle feedback survey.

### Step 0 - Download this repository

Execute:

```
git clone https://gitlab.com/pdagog/moodlenq.git
```

or download the [ZIP file from
gitlab](https://gitlab.com/pdagog/moodlenq/-/archive/master/moodlenq-master.zip)
and extract files.



### Step 1 - Build The Specification

The first step is to build the questionnaire specification as
an Excel file. See [next section](#specification-file) for a
description of the specification file format. You can also use an
[example](example-en/spec.xlsx).

Assuming the specification file is named `spec.xlsx`, run the following
command to generate the XML import file (named `eval.xml` here)
for Moodle:

```
./moodlenq.py xml -o eval.xml spec.xlsx
```

You can now import this XML file into Moodle (click on Feedback, then
select the `Templates` tab, then select `Import questions`), and check
how your imported file looks like with the `Edit questions` tab.


### Step 2 - Announce The Survey To Students

The next step is to announce availability of the survey to students.
Leave them some time, and send a reminder before closing the survey.


### Step 3 - Create a Skeleton LaTeX File

Meanwhile, you can create a skeleton LaTeX file from the specification.

```
./moodlenq.py skel -o eval.tex spec.xlsx
```

You can modify the generated `eval.tex` file according to your taste
and the data provided by students. However, you cannot compile this file
until survey graphs and data are generated according to the next step.


### Step 4 - Export Answers And Generate the Document

The final step is to export data generated by the survey with Moodle:
in the `Analysis` tab, click on the `Export to Excel` button and use
the command:

```
./moodlenq.py result spec.xlsx feedback.xlsx
```

The program generates a lot of PDF and TEX files (one by question) whose
name begins with `q` (so you can remove them easily in one command).
These files are all included by the generated (in step 3) `eval.tex`.

You can now run:

```
pdflatex eval.tex
```

to get the final file. You can repeat the step 4 as many times as
necessary.


Specification File
------------------

The first step is to write the questionnaire specification as an Excel
file (see an [example here](example-en/spec.xlsx)). This file must contain
one sheet and the following columns:

| Column | Description |
| :----: | :--------- |
|   A    | type (see below)       |
|   B    | attributes (see below) |
|   C    | question text for Moodle |
|   D    | question text for LaTeX |
|  E and following | couples (legend, color) on 2 consecutive columns |


The specification really begins at the first row where column A is a
valid type. This allows you to put some comments (such as column headers)
in the first row(s) of the file.


### Column A - type

The **type** is one of:

| Type         | Description | Attributes supported |
| :----------: | :---------- | :------------------- |
| `lang`       | this pseudo-type specifies (via the attribute in column B) the language to use in the final document | `fr`, `en` |
| `title`      | specifies a title in the questionnaire | (none) |
| `text`       | specifies a line of text (without answer) in the questionnaire | `bold` |
| `choice`     | specifies a choice (via radio buttons) between values cited in columns E, G, I and so on | `bold`, `req`, `hor`/`vert`, `noanswer` |
| `menu`       | specifies a choice (via a drop-down menu) between values cited in columns E, G, I and so on | `req`, `noanswer` |
| `multichoice`| specifies a multiple choice (via checkboxes) with values cited in columns E, G, I and so on | `bold`, `req`, `hor`/`vert`, `noanswer` |
| `comment`    | specifies a multi-line comment | `req`, *ncol*`\|`*nrow* |
| `line`       | specifies a short, single-line comment | `req`, *max*`\|`*width* |
| `numeric`    | specifies a numeric value input field | `req`, *min*`\|`*max* |


### Column B - attributes

Available **attributes** (in column B) are:

| Attribute | Description |
| :-------: | :---------- |
| `fr` or `en` | for `lang` only: provides  the language used in texts |
| `req`     | input required |
| `bold`    | choices (or single line text) are in bold |
| `hor`     | `choice` and `multichoice` items are presented horizontally (default) |
| `vert`    | `choice` and `multichoice` items are presented vertically |
| `noanswer`| show the special value `no answer` as a possible choice |
| *ncol*`\|`*nrow* | for `comment` type, input area is a text area of *ncol* columns by *nrow* rows |
| *max*`\|`*width* | for `line` type, input area is a single line area of *max* input characters displayed in a *width* columns area |
| *min*`\|`*max* | for `numeric` type, input area allows numbers between *min* and *max* |
| `cumhbar=`*key* | group multiple `choice` values into a single (horizontal) stacked bar histogram |
| `cumvbar=`*key* | group multiple `choice` values into a single (vertical) stacked bar histogram |
| `rotlbl=`*angle* | angle for axis legend rotation |
| `reverse=`*1 or 0* | reverse (or don't change) items in each stacked bar |

Unrecognized attributes are silently discarded.


### Column C and D - Question texts

Column C specifies the question text to use in the Moodle questionnaire
(including HTML marks if you want).

You can use a different text for the LaTeX generated files: by default,
the text is the same than the Moodle question text. However, if it
includes HTML marks or if you want shorter texts (especially for graph
titles), you can include a different text.


### Column E and after - Legends and colors

The next columns are used by pairs: the first column is the legend text
and the next one is the color.

Color are specified by the following values:

| Color | Description |
| :---: | :---------- |
| *color name* | use this color name |
| `#rrggbb` | use this color numeric specification |
| (empty) | use a default color for this item |
| `/`   | use a color obtained by shading surrounding item colors |


### Example

This example shows a sample specification:

| A             | B          | C | D | E | F | G | H | I | J |
|:------------- |:---------- |:--|:--|:--|:--|:--|:--|:--|:--|
| `Type`        | `Attr`     | `Moodle title`       | `LaTeX title` | `legend1` | `color1` | `legend2` | `color2` | ... | ... |
| `lang`        | `en`       |                      |              | | | | | | |
| `title`       |            | `About you`          |              | | | | | | |
| `text`        | `bold`     | `Answer honestly`    |              | | | | | | |
| `choice`      | `req bold` | `Are you happy`      | `Happyness`  | `Sure` | `green`  | `May be`  | `/` | `No`  | `red` |
| `multichoice` |            | `What motivates you` | `Motivation` | `Money` | | `Knowledge` | | | |
| `choice`      | `cumhbar=foo` | `Chapter 1 difficulty (0: very easy, 2: very difficult)` | `Chap 1`  | `0` | `green`  | `1`  | `/` | `2`  | `red` |
| `choice`      | `cumhbar=foo` | `Chapter 2 difficulty (0: very easy, 2: very difficult)` | `Chap 2`  | `0` | `green`  | `1`  | `/` | `2`  | `red` |
| `choice`      | `cumhbar=foo` | `Chapter 3 difficulty (0: very easy, 2: very difficult)` | `Chap 3`  | `0` | `green`  | `1`  | `/` | `2`  | `red` |
| `comment`     | `80\|10`    | `Explain why`       |              | | | | | | |

You may also find a [complete example here](example-en/spec.xlsx).


Release notes
-------------

### v2.2

* fix bug when reporting a message

### v2.1

* fix bug when a legend is a number (numeric type)

### v2.0

* add the `-V` to get version number
* use **openpyxl** instead of the obsolete **xlrd**. As a result,
  old `xls` (Excel <= 2007) are no longer recongnized, only `.xlsx`
  (Excel >= 2010) can be read.
    * One can use `libreoffice --convert-to xlsx spec.xls` to
      automatically convert old `.xls` files to new `.xlsx`
* fix some regular expressions no longer valid on recent Python versions.

### v1.4

* wrap graph titles if needed

### v1.3

* respect parkip in \\figcomment in the LaTeX document

### v1.2

* add reverse stack bars (atttribute `reverse=`)

### v1.1

* add cumulated histograms (attributes `cumhbar=` and `cumvbar=`)
* add `rotlbl=` attribute (for `cumhbar=` and `cumvbar=` only at this time)

### v1.0

* Initial release
